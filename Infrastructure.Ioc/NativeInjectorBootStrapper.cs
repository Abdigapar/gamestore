﻿using Application.Game.Interfaces;
using Application.Game.Services;
using Domain.Core.Bus;
using Domain.Core.Events;
using Domain.Core.Notifications;
using Domain.Data.CommandHandlers;
using Domain.Data.Commands;
using Domain.Data.EventHandlers;
using Domain.Data.Events;
using Domain.Data.Interfaces;
using Infrastructure.Bus;
using Infrastructure.Data.Context;
using Infrastructure.Data.EventSourcing;
using Infrastructure.Data.Repository;
using Infrastructure.Data.Repository.EventSourcing;
using Infrastructure.Data.UoW;
using Infrastructure.Identity.Authorization;
using Infrastructure.Identity.Models;
using Infrastructure.Identity.Services;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;

namespace Infrastructure.Ioc
{
    public class NativeInjectorBootStrapper
    {
        public static void RegisterServices(IServiceCollection services)
        {
            // ASP.NET HttpContext dependency
            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();

            // Domain Bus (Mediator)
            services.AddScoped<IMediatorHandler, InMemoryBus>();

            // ASP.NET Authorization Polices
            services.AddSingleton<IAuthorizationHandler, ClaimsRequirementHandler>();

            //Application 
            services.AddScoped<IGameAppService, GameAppService>();

            // Domain - Events
            services.AddScoped<INotificationHandler<DomainNotification>, DomainNotificationHandler>();

            //Game - events
            services.AddScoped<INotificationHandler<GameRegisteredEvent>, GameEventHandler>();
            services.AddScoped<INotificationHandler<GameUpdatedEvent>, GameEventHandler>();
            services.AddScoped<INotificationHandler<GameRemovedEvent>, GameEventHandler>();

            //Game - Commands
            services.AddScoped<IRequestHandler<RegisterNewGameCommand>, GameCommandHandler>();
            services.AddScoped<IRequestHandler<UpdateGameCommand>, GameCommandHandler>();
            services.AddScoped<IRequestHandler<RemoveGameCommand>, GameCommandHandler>();

            // Infra - Data
            services.AddScoped<IGameRepository, GameRepository>();
            services.AddScoped<IUnitOfWork, UnitOfWork>();
            services.AddScoped<GameContext>();

            // Infra - Data EventSourcing
            services.AddScoped<IEventStoreRepository, EventStoreSQLRepository>();
            services.AddScoped<IEventStore, SqlEventStore>();
            services.AddScoped<EventStoreSqlContext>();

            // Infra - Identity Services
            services.AddTransient<IEmailSender, AuthEmailMessageSender>();
            services.AddTransient<ISmsSender, AuthSmsMessageSender>();

            // Infra - Identity
            services.AddScoped<IUser, AspNetUser>();
        }
    }
}