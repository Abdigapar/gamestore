﻿namespace Application.Game.Services
{
    using System;
    using System.Collections.Generic;
    using EventSourcedNormalizers;
    using Interfaces;
    using ViewModels;
    using global::AutoMapper;
    using global::AutoMapper.QueryableExtensions;
    using Domain.Core.Bus;
    using Domain.Data.Commands;
    using Domain.Data.Interfaces;
    using Infrastructure.Data.Repository.EventSourcing;
    public class GameAppService : IGameAppService
    {
        private readonly IMapper _mapper;
        private readonly IGameRepository _gameRepository;
        private readonly IEventStoreRepository _eventStoreRepository;
        private readonly IMediatorHandler _bus;

        public GameAppService(IMapper mapper,
            IGameRepository gameRepository,
            IEventStoreRepository eventStoreRepository,
            IMediatorHandler bus)
        {
            _mapper = mapper;
            _gameRepository = gameRepository;
            _eventStoreRepository = eventStoreRepository;
            _bus = bus;
        }

        public void Register(GameViewModel gameViewModel)
        {
            var registerCommand = _mapper.Map<RegisterNewGameCommand>(gameViewModel);
            _bus.SendCommand(registerCommand);
        }

        public void Update(GameViewModel gameViewModel)
        {
            var updateCommand = _mapper.Map<UpdateGameCommand>(gameViewModel);
            _bus.SendCommand(updateCommand);
        }

        public void Remove(Guid id)
        {
            var removeCommand = new RemoveGameCommand(id);
            _bus.SendCommand(removeCommand);
        }

        public IEnumerable<GameViewModel> GetAll()
        {
            return _gameRepository.GetAll().ProjectTo<GameViewModel>();
        }

        public GameViewModel GetById(Guid id)
        {
            return _mapper.Map<GameViewModel>(_gameRepository.GetById(id));
        }

        public IList<GameHistoryData> GetAllHistory(Guid id)
        {
            return GameHistory.ToJavaScriptGameHistory(_eventStoreRepository.All(id));
        }

        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }
    }
}