﻿namespace Application.Game.EventSourcedNormalizers
{
    public class GameHistoryData
    {
        public string Action { get; set; }
        public string Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Category { get; set; }
        public string Price { get; set; }
        public string When { get; set; }
        public string Who { get; set; }
    }
}