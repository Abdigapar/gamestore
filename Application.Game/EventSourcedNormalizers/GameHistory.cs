﻿using System;
using System.Collections.Generic;
using System.Linq;
using Domain.Core.Events;
using Newtonsoft.Json;

namespace Application.Game.EventSourcedNormalizers
{
    public class GameHistory
    {
        public static IList<GameHistoryData> HistoryData { get; set; }

        public static IList<GameHistoryData> ToJavaScriptGameHistory(IList<StoredEvent> storedEvents)
        {
            HistoryData = new List<GameHistoryData>();
            GameHistoryDeserializer(storedEvents);

            var sorted = HistoryData.OrderBy(c => c.When);
            var list = new List<GameHistoryData>();
            var last = new GameHistoryData();

            foreach (var change in sorted)
            {
                var jsSlot = new GameHistoryData
                {
                    Id = change.Id == Guid.Empty.ToString() || change.Id == last.Id
                        ? ""
                        : change.Id,
                    Name = string.IsNullOrWhiteSpace(change.Name) || change.Name == last.Name
                        ? ""
                        : change.Name,
                    Description = string.IsNullOrWhiteSpace(change.Description) || change.Description == last.Description
                        ? ""
                        : change.Description,
                    Category = string.IsNullOrWhiteSpace(change.Category) || change.Price == last.Price
                        ? ""
                        : change.Category,
                    Price = string.IsNullOrWhiteSpace(change.Price) || change.Price == last.Price
                        ? ""
                        : change.Price,
                    Action = string.IsNullOrWhiteSpace(change.Action) ? "" : change.Action,
                    When = change.When,
                    Who = change.Who
                };

                list.Add(jsSlot);
                last = change;
            }
            return list;
        }

        private static void GameHistoryDeserializer(IEnumerable<StoredEvent> storedEvents)
        {
            foreach (var e in storedEvents)
            {
                var slot = new GameHistoryData();
                dynamic values;

                switch (e.MessageType)
                {
                    case "GameRegisteredEvent":
                        values = JsonConvert.DeserializeObject<dynamic>(e.Data);
                        slot.Id = values["Id"];
                        slot.Name = values["Name"];
                        slot.Description = values["Description"];
                        slot.Category = values["Category"];
                        slot.Price = values["Price"];
                        slot.Action = "Registered";
                        slot.When = values["Timestamp"];
                        slot.Who = e.User;
                        break;

                    case "GameUpdateEvent":
                        values = JsonConvert.DeserializeObject<dynamic>(e.Data);
                        slot.Id = values["Id"];
                        slot.Name = values["Name"];
                        slot.Description = values["Description"];
                        slot.Category = values["Category"];
                        slot.Price = values["Price"];
                        slot.Action = "Registered";
                        slot.When = values["Timestamp"];
                        slot.Who = e.User;
                        break;

                    case "GameRemovedEvent":
                        values = JsonConvert.DeserializeObject<dynamic>(e.Data);
                        slot.Action = "Removed";
                        slot.When = values["Timestamp"];
                        slot.Id = values["Id"];
                        slot.Who = e.User;
                        break;
                }
                HistoryData.Add(slot);
            }
        }
    }
}