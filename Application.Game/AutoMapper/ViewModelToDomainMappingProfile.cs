﻿namespace Application.Game.AutoMapper
{
    using ViewModels;
    using global::AutoMapper;
    using Domain.Data.Commands;
    public class ViewModelToDomainMappingProfile : Profile
    {
        public ViewModelToDomainMappingProfile()
        {
            CreateMap<GameViewModel, RegisterNewGameCommand>()
                .ConstructUsing(c => new RegisterNewGameCommand(c.Name, c.Description, c.Category, c.Price));
            CreateMap<GameViewModel, UpdateGameCommand>()
                .ConstructUsing(c => new UpdateGameCommand(c.Name, c.Description, c.Category, c.Price));
        }
    }
}