﻿namespace Application.Game.AutoMapper
{
    using ViewModels;
    using global::AutoMapper;
    using Domain.Data.Models;
    public class DomainToViewModelMappingProfile : Profile
    {
        public DomainToViewModelMappingProfile()
        {
            CreateMap<Game, GameViewModel>();
        }
    }
}