﻿using System;
using System.Collections.Generic;
using Application.Game.EventSourcedNormalizers;
using Application.Game.ViewModels;

namespace Application.Game.Interfaces
{
    public interface IGameAppService : IDisposable
    {
        void Register(GameViewModel gameViewModel);
        void Update(GameViewModel gameViewModel);
        void Remove(Guid id);
        IEnumerable<GameViewModel> GetAll();
        GameViewModel GetById(Guid id);
        IList<GameHistoryData> GetAllHistory(Guid id);
    }
}