﻿using System;
using System.Collections.Generic;
using Domain.Core.Events;

namespace Infrastructure.Data.Repository.EventSourcing
{
    public interface IEventStoreRepository
    {
        void Store(StoredEvent theEvent);
        IList<StoredEvent> All(Guid aggregateId);
    }
}