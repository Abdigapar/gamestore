﻿using Domain.Data.Interfaces;
using Infrastructure.Data.Context;

namespace Infrastructure.Data.UoW
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly GameContext _context;

        public UnitOfWork(GameContext context)
        {
            _context = context;
        }

        public bool Commit()
        {
            return _context.SaveChanges() > 0;
        }

        public void Dispose()
        {
            _context.Dispose();
        }
    }
}