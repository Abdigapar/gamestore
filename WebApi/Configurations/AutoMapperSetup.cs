﻿namespace WebApi.Configurations
{
    using System;
    using Application.Game.AutoMapper;
    using AutoMapper;
    using Microsoft.Extensions.DependencyInjection;
    public static class AutoMapperSetup
    {
        public static void AddAutoMapperSetup(this IServiceCollection services)
        {
            if (services == null) throw new ArgumentNullException(nameof(services));

            services.AddAutoMapper();

            AutoMapperConfig.RegisterMappings();
        }
    }
}