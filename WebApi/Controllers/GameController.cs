﻿using System;
using System.Collections.Generic;
using Application.Game.Interfaces;
using Application.Game.ViewModels;
using Domain.Core.Bus;
using Domain.Core.Notifications;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace WebApi.Controllers
{

    public class GameController : ApiController
    {
        private readonly IGameAppService _gameAppService;

        public GameController(
            IGameAppService gameAppService,
            INotificationHandler<DomainNotification> notification,
            IMediatorHandler mediator) : base(notification, mediator)
        {
            _gameAppService = gameAppService;
        }

        [HttpGet]
        [AllowAnonymous]
        [Route("game-management")]
        public ActionResult<IEnumerable<string>> Get()
        {
            return new string[] {"value1"};
            //return Response(_gameAppService.GetAll());
        }

        [HttpGet]
        [AllowAnonymous]
        [Route("game-management/{id:guid}")]
        public IActionResult Get(Guid id)
        {
            var gameViewModel = _gameAppService.GetById(id);
            return Response(gameViewModel);
        }

        [HttpPost]
        [Route("game-management")]
        public IActionResult Post([FromBody] GameViewModel gameViewModel)
        {
            if (!ModelState.IsValid)
            {
                NotifyModelStateErrors();
                return Response(gameViewModel);
            }

            _gameAppService.Register(gameViewModel);

            return Response(gameViewModel);
        }

        [HttpPut]
        [Route("game-management")]
        public IActionResult Put([FromBody] GameViewModel gameViewModel)
        {
            if (!ModelState.IsValid)
            {
                NotifyModelStateErrors();
                return Response(gameViewModel);
            }

            _gameAppService.Update(gameViewModel);

            return Response(gameViewModel);
        }

        [HttpDelete]
        [Route("game-management")]
        public IActionResult Delete(Guid id)
        {
            _gameAppService.Remove(id);
            return Response();
        }

        [HttpGet]
        [Route("game-management/history/{id:guid}")]
        public IActionResult History(Guid id)
        {
            var gameHisotryData = _gameAppService.GetAllHistory(id);
            return Response(gameHisotryData);
        }
    }
}