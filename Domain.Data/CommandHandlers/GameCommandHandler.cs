﻿namespace Domain.Data.CommandHandlers
{
    using System;
    using System.Threading;
    using System.Threading.Tasks;
    using Core.Bus;
    using Core.Notifications;
    using Commands;
    using Events;
    using Interfaces;
    using Models;
    using MediatR;
    public class GameCommandHandler : CommandHandler,
        IRequestHandler<RegisterNewGameCommand>,
        IRequestHandler<UpdateGameCommand>,
        IRequestHandler<RemoveGameCommand>
    {
        private readonly IGameRepository _gameRepository;
        private readonly IMediatorHandler _bus;

        public GameCommandHandler(IGameRepository gameRepository,
            IUnitOfWork uow,
            IMediatorHandler bus,
            INotificationHandler<DomainNotification> notification) : base(uow, bus, notification)
        {
            _gameRepository = gameRepository;
            _bus = bus;
        }

        //Задача для регистраций 
        public Task Handle(RegisterNewGameCommand message, CancellationToken cancellationToken)
        {
            if (!message.IsValid())
            {
                NotifyValidationErrors(message);
                return Task.CompletedTask;
            }

            var game = new Game(Guid.NewGuid(), message.Name, message.Description, message.Category, message.Price,
                message.ImageData, message.ImageMimeType);

            if (_gameRepository.GetByName(game.Name) != null)
            {
                _bus.RaiseEvent(new DomainNotification(message.MessageType, "Игра с таким названием уже есть"));
                return Task.CompletedTask;
            }

            _gameRepository.Add(game);

            if (Commit())
            {
                _bus.RaiseEvent(new GameRegisteredEvent(game.Id, game.Name, game.Description, game.Category, game.Price,
                    game.ImageData, game.ImageMimeType));
            }

            return Task.CompletedTask;
        }

        //Задача при обновлений 
        public Task Handle(UpdateGameCommand message, CancellationToken cancellationToken)
        {
            if (!message.IsValid())
            {
                NotifyValidationErrors(message);
                return Task.CompletedTask;
            }

            var game = new Game(Guid.NewGuid(), message.Name, message.Description, message.Category, message.Price,
                message.ImageData, message.ImageMimeType);
            var existingGame = _gameRepository.GetByName(game.Name);

            if (existingGame != null && existingGame.Id != game.Id)
            {
                if (!existingGame.Equals(game))
                {
                    _bus.RaiseEvent(new DomainNotification(message.MessageType, " Игра с таким названием уже есть"));
                    return Task.CompletedTask;
                }
            }

            _gameRepository.Update(game);

            if (Commit())
            {
                _bus.RaiseEvent(new GameUpdatedEvent(game.Id, game.Name, game.Description, game.Category, game.Price,
                    game.ImageData, game.ImageMimeType));
            }

            return Task.CompletedTask;
        }

        //Задача для удаление 
        public  Task Handle(RemoveGameCommand message, CancellationToken cancellationToken)
        {
            if (!message.IsValid())
            {
                NotifyValidationErrors(message);
                return  Task.CompletedTask;
            }

            _gameRepository.Remove(message.Id);

            if (Commit())
            {
                _bus.RaiseEvent(new GameRemovedEvent(message.Id));
            }

            return  Task.CompletedTask;
        }

        //Очистка мусора
        public void Dispose()
        {
            _gameRepository.Dispose();
        }
    }
}