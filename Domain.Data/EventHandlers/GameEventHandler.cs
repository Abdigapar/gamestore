﻿namespace Domain.Data.EventHandlers
{
    using System.Threading;
    using System.Threading.Tasks;
    using Events;
    using MediatR;
    public class GameEventHandler : 
        INotificationHandler<GameRegisteredEvent>,
        INotificationHandler<GameUpdatedEvent>,
        INotificationHandler<GameRemovedEvent>

    {
        public Task Handle(GameRegisteredEvent message, CancellationToken cancellationToken)
        {
            return Task.CompletedTask;
        }
        public Task Handle(GameUpdatedEvent message, CancellationToken cancellationToken)
        {
            return Task.CompletedTask;
        }
        public Task Handle(GameRemovedEvent message, CancellationToken cancellationToken)
        {
            return Task.CompletedTask;
        }
    }
}