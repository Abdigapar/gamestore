﻿namespace Domain.Data.Validations
{
    using System;
    using Commands;
    using FluentValidation;
    public abstract class GameValidation<T> : AbstractValidator<T> where T: GameCommand
    {
        protected void ValidateName()
        {
            RuleFor(c => c.Name)
                .NotEmpty().WithMessage("Введите название игры");
        }

        protected void ValidateDescription()
        {
            RuleFor(c => c.Description)
                .NotEmpty().WithMessage("Введите описание игры");
        }

        protected void ValidateCategory()
        {
            RuleFor(c => c.Category)
                .NotEmpty().WithMessage("Введите категорию игры");
        }

        protected void ValidatePrice()
        {
            RuleFor(c => c.Price)
                .NotEmpty().WithMessage("Введите цену");
        }

        protected void ValidateId()
        {
            RuleFor(c => c.Id)
                .NotEqual(Guid.Empty);
        }
    }
}