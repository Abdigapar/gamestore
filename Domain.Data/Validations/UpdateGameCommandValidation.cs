﻿using Domain.Data.Commands;

namespace Domain.Data.Validations
{
    public class UpdateGameCommandValidation : GameValidation<UpdateGameCommand>
    {
        public UpdateGameCommandValidation()
        {
            ValidateId();
            ValidateName();
            ValidateDescription();
            ValidateCategory();
            ValidatePrice();
        }
    }
}