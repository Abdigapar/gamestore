﻿namespace Domain.Data.Validations
{
    using Commands;
    public class RegisterNewGameCommandValidation : GameValidation<RegisterNewGameCommand>
    {
        public RegisterNewGameCommandValidation()
        {
            ValidateName();
            ValidateDescription();
            ValidateCategory();
            ValidatePrice();
        }
    }
}