﻿namespace Domain.Data.Validations
{
    using Commands;
    public class RemoveGameCommandValidation : GameValidation<RemoveGameCommand>
    {
        public RemoveGameCommandValidation()
        {
            ValidateId();
        }
    }
}