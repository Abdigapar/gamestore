﻿using System;
using Domain.Data.Validations;

namespace Domain.Data.Commands
{
    public class RemoveGameCommand : GameCommand
    {
        public RemoveGameCommand(Guid id)
        {
            Id = id;
        }

        public override bool IsValid()
        {
            ValidationResult = new RemoveGameCommandValidation().Validate(this);
            return ValidationResult.IsValid;
        }
    }
}