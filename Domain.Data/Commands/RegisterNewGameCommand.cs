﻿using Domain.Data.Validations;

namespace Domain.Data.Commands
{
    public class RegisterNewGameCommand : GameCommand
    {
        public RegisterNewGameCommand ( string name, string description, string category, decimal price)
        {
            Name = name;
            Description = description;
            Category = category;
            Price = price;
          
        }
        public override bool IsValid()
        {
            ValidationResult = new RegisterNewGameCommandValidation().Validate(this);
            return ValidationResult.IsValid;
        }

    }
}