﻿using Domain.Data.Validations;

namespace Domain.Data.Commands
{
    public class UpdateGameCommand : GameCommand
    {
        public UpdateGameCommand(string name, string description, string category, decimal price)
        {
            Name = name;
            Description = description;
            Category = category;
            Price = price;
        }

        public override bool IsValid()
        {
            ValidationResult = new UpdateGameCommandValidation().Validate(this);
            return ValidationResult.IsValid;
        }
    }
}