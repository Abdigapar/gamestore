﻿using System;
using Domain.Core.Events;

namespace Domain.Data.Events
{
    public class GameRemovedEvent: Event
    {
        public GameRemovedEvent(Guid id)
        {
            Id = id;
            AggregateId = id;
        }

        public Guid Id { get; set; }
    }
}