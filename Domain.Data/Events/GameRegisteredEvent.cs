﻿using System;
using Domain.Core.Events;

namespace Domain.Data.Events
{
    public class GameRegisteredEvent : Event
    {
        public GameRegisteredEvent(Guid id, string name, string description, string category, decimal price, byte[] imageData, string imageMimeType)
        {
            Id = id;
            Name = name;
            Description = description;
            Category = category;
            Price = price;
            ImageData = imageData;
            ImageMimeType = imageMimeType;
            AggregateId = id;
        }

        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Category { get; set; }
        public decimal Price { get; set; }
        public byte[] ImageData { get; set; }
        public string ImageMimeType { get; set; }
    }
}