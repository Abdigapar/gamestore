﻿using Domain.Data.Models;

namespace Domain.Data.Interfaces
{
    public interface IGameRepository : IRepository<Game>
    {
        Game GetByName(string name);
    }
}